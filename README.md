# [dotfiles-mpv](https://gitlab.com/eidoom/dotfiles-mpv)

[Companion post](https://eidoom.gitlab.io/computing-blog/post/linux-video/)

Install with

```shell
git clone git@gitlab.com:eidoom/dotfiles-mpv.git
./install.sh
```

[Reference manual](https://mpv.io/manual/stable/)
